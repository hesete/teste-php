<?php namespace App\Controllers;

use App\libraries\FileProcessor\FileProcessorRelatoryWeb as Processor;

class Home extends BaseController
{
	public function index(){
		helper('form');
		$data = [
			'page_title' => 'Home'
		];
		
		return view('home',$data);
	}

	public function upload(){
		helper(['form','url','filesystem']);
		$data = ['page_title' => 'Home'];
		$pathFilesUpload = WRITEPATH.'data/in';
		$pathRelatory = WRITEPATH.'data/out';

		if($files = $this->request->getFiles()){
			if (! $this->validate(['files' => 'uploaded[files]'])){
				$data['validation'] = $this->validator;
			}else{
				foreach($files['files'] as $file){
					if ($file->isValid() && ! $file->hasMoved()){
						$file->move($pathFilesUpload);
						$fileOut = $pathRelatory.'/'.preg_replace('/(\.dat$)/','.done.dat',$file->getName());
						$fileProcessor = new Processor();
						$fileProcessor->process($pathFilesUpload.'/'.$file->getName(),$fileOut);
						$data['relatory'][]= ['file'=>$file->getName(),'text'=>$fileProcessor->relatoryFormat()];
						write_file($fileOut, $fileProcessor->relatoryFormat());
					}
				}
			}
        }
		return view('home',$data);
	}


	//--------------------------------------------------------------------

}
