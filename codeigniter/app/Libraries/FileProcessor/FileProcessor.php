<?php
namespace App\libraries\FileProcessor;

class FileProcessor{
    protected $prepareStringSearch = array ('/(, | ,)/','/(\. | \.)/','/(-­|­-|--|\xC2\xAD)/','/(  )/','/(\[|\])/');
    protected $prepareStringReplace = array (',','.','-',' ','"');
    
    public function process($filein,$fileout){
        $rows = array_map(array($this,"prepareString"), file($filein));
        foreach($rows as $row){
            list($entity) = explode(',',$row);
            if(method_exists($this,'process_'.$entity)){
                call_user_func(array($this,'process_'.$entity),$row);
            }else{
                $this->entityMethodNotImplented('process_'.$entity);
            }
        }
        $this->endProcess($filein,$fileout);
    }
    
    protected function prepareString($string){
        return preg_replace($this->prepareStringSearch, $this->prepareStringReplace, $string);
    }

    protected function endProcess($filein,$fileout){
        print("Method not implemented: endProcess(\$filein,\$fileout) \n");
    }

    protected function entityMethodNotImplented($method){
        print("Method not implemented: ".$method."\n");
    }
}

?>