<?php
namespace App\libraries\FileProcessor;

require_once 'FileProcessorEntityUntil003.php';

class FileProcessorRelatory extends FileProcessorEntityUntil003{
    
    protected function relatoryFormat(){
        $data="";
        foreach($this->relatory as $item){
            $data = $data.$item[0].": ".$item[1]."\n";
        }
        return $data;
    }

    protected function writeFile($fileout){
        file_put_contents($fileout,$this->relatoryFormat());
    }

    protected function endProcess($filein,$fileout){
        $this->processRelatory();
        $this->writeFile($fileout);
    }
}

?>