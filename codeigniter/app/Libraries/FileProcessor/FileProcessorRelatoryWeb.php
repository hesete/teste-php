<?php
namespace App\libraries\FileProcessor;

require_once 'FileProcessorEntityUntil003.php';

class FileProcessorRelatoryWeb extends FileProcessorEntityUntil003{
    
    public function relatoryFormat(){
        $data="";
        foreach($this->relatory as $item){
            $data = $data.$item[0].": ".$item[1]."\n";
        }
        return $data;
    }
    
    protected function writeFile($fileout){
        file_put_contents($fileout,$this->relatoryFormat());
    }

    public function endProcess($filein,$fileout){
        $this->processRelatory();
    }
    
}

?>