<?php
namespace App\libraries\FileProcessor;

class SaleItem{
    private $string;
    private $itemId;
    private $itemQuantity;
    private $itemPrice;

    public function __construct($string){
        $this->string = $string;
        $this->parse();
	}

	public function parse(){
        list($itemId,$itemQuantity,$itemPrice) = explode('-',$this->string);
        $this->itemId = $itemId;
        $this->itemQuantity = (float)$itemQuantity;
        $this->itemPrice = (float)$itemPrice;
    }

    public function getItemId(){
        return $this->itemId;
    }

    public function getItemQuantity(){
        return $this->itemQuantity;
    }

    public function getItemPrice(){
        return $this->itemPrice;
    }
}

?>