<?php
namespace App\libraries\FileProcessor;

class Salesman{
    private $string;
	private $cpf;
	private $name;
	private $salary;
	private $totalSales=0;

	public function __construct($string){
		$this->string = $string;
        $this->parse();
	}

	public function parse()
	{
		list($ent,$cpf,$name,$salary)=str_getcsv($this->string,',','"');
		
		$this->cpf = $cpf;
		$this->name = $name;
		$this->salary = (float)$salary;
	}

    public function getCpf(){
        return $this->cpf;
    }
    
    public function getName(){
        return $this->name;
    }

    public function getSalary(){
        return $this->salary;
	}
	
	public function sumSale($val){
		$this->totalSales += $val;
	}

	public function getTotalSales(){
		return $this->totalSales;
	}
}
?>