<header>
  <div class="collapse bg-dark" id="navbarHeader">
	<div class="container">
	  <div class="row">
		<div class="col-sm-8 col-md-7 py-4">
		  <h4 class="text-white">Sobre Helio Seichi Terashima</h4>
		  <p class="text-muted">
		  Sou um desenvolvedor web com mais de 7 anos de experiencia. 
		  Trabalhei com instalação e manutenção de redes de computadores e servidores. 
		  Tenho interesse em melhores praticas de desenvolvimento, infraestrutura e escalabilidade de aplicações.
		  </p>
		  <p class="text-muted">09/01/2021</p>
		</div>
		<div class="col-sm-4 offset-md-1 py-4">
		  <h4 class="text-white">Contact</h4>
		  <ul class="list-unstyled">
			<li><a href="https://gitlab.com/hesete" target="blank" class="text-white">Gitlab</a></li>
		  </ul>
		</div>
	  </div>
	</div>
  </div>
  <div class="navbar navbar-dark bg-dark box-shadow">
	<div class="container d-flex justify-content-between">
	  <a href="#" class="navbar-brand d-flex align-items-center">
		<strong><?=$page_title?></strong>
	  </a>
	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarHeader" aria-controls="navbarHeader" aria-expanded="false" aria-label="Alternar de navegação">
		<span class="navbar-toggler-icon"></span>
	  </button>
	</div>
  </div>
</header>
