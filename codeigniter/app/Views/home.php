<?= $this->extend('default') ?>

<?= $this->section('content') ?>
<section class="jumbotron">
	<div class="container">
		<?=form_open_multipart(base_url('home/upload'), 'class="was-validated"');?>
			<div id="copie">
				<div class="form-row">
					<div class="col-md-10">
						<input name="files[]" type="file" class="custom-file-input" accept=".dat" id="" required>
						<label class="custom-file-label" for="">Selecionar Arquivo .dat</label>
					</div>
				</div>
			</div>

			<div id="copied"></div>
			<div class="form-row">
				<button id="add" class="btn btn-primary" type="button">Add File</button>
			</div>
			<div class="form-row">
				<button class="btn btn-primary" type="submit">Enviar</button>
			</div>
		</form>
	</div>
</section>
<script type="text/javascript">
    $(function(){
		$("#add").click(()=>{
			var div = $($('#copie').html()).appendTo('#copied');
			var div_btn = $('<div class="col-md-2"><button class="btn btn-primary btn_del" type="button">Del File</button></div>').appendTo(div);
			$(div_btn,'button').click(()=>{
				$(div_btn).parent().remove()
				$("#add").removeAttr( "disabled" );
			});
			if($('#copied').children().length>4){
				$("#add").attr("disabled","disabled");
			}
		});
    });
</script>
<div class="album py-5 bg-light">
	<div class="container">
		<div class="row">
			<?php if(isset($relatory)) foreach($relatory as $file){?>
			<div class="col-md-4">
				<div class="card mb-4 box-shadow">
				<div class="card-body">
					<p class="card-text"><?=$file['text']?></p>
					<div class="d-flex justify-content-between align-items-center">
					<small class="text-muted"><?=$file['file']?></small>
					</div>
				</div>
				</div>
			</div>
			<?php } ?>
		</div>
	</div>
</div>

<?= $this->endSection() ?>