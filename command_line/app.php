<?php
namespace App\libraries\FileProcessor;

require_once "lib/FileProcessorRelatory.php";


$dirIn = "data/in/";
$dirOut = "data/out/";
$extensionIn = "*.dat";
$extensionOut = ".done.dat";

foreach(glob($dirIn.$extensionIn) as $fileIn){
    $fileName = pathinfo($fileIn,PATHINFO_FILENAME);
    $fileOut = $dirOut.$fileName.$extensionOut;
    $fileProcessor = new FileProcessorRelatory();
    $fileProcessor->process($fileIn,$fileOut);
    print_r("Processed: ".$fileOut."\n");
} 

?>