<?php
namespace App\libraries\FileProcessor;

class Customer{
    private $string;
	private $cnpj;
	private $name;
	private $businessArea;

	public function __construct($string){
		$this->string = $string;
        $this->parse();
	}

	public function parse()
	{
		list($ent,$cnpj,$name,$businessArea)=str_getcsv($this->string,',','"');
		
		$this->cnpj = $cnpj;
		$this->name = $name;
		$this->businessArea = $businessArea;
	}

    public function getCnpj(){
        return $this->cnpj;
    }

    public function getName(){
        return $this->name;
    }

    public function getBusinessArea(){
        return $this->businessArea;
    }
}
?>