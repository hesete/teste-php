<?php
namespace App\libraries\FileProcessor;

require_once 'Customer.php';
require_once 'Sale.php';
require_once 'Salesman.php';
require_once 'FileProcessor.php';


class FileProcessorEntityUntil003 extends FileProcessor{
    protected $customer = [];
    protected $sale = [];
    protected $salesman = [];
    protected $salarySum = 0;
    protected $bestSale = null;
    protected $relatory=[];
    
    protected function process_001($row){
        $objSalesman = new Salesman($row);
        if (!isset($salesman[$objSalesman->getName()])){
            $this->salesman[$objSalesman->getName()] = $objSalesman;
            $this->salarySum += $objSalesman->getSalary();
        }
    }

    protected function process_002($row){
        $objCustomer = new Customer($row);
        if (!isset($customer[$objCustomer->getCnpj()])){
            $this->customer[$objCustomer->getCnpj()] = $objCustomer;
        }
    }
    
    protected function process_003($row){
        $objSale = new Sale($row);
        if (!isset($customer[$objSale->getSaleId()])){
            $this->sale[$objSale->getSaleId()] = $objSale;
        }
        $this->salesman[$objSale->getSalesman()]->sumSale($objSale->getTotal());
        if($this->bestSale==null){
            $this->bestSale = $objSale;
        }else if($objSale->getTotal()>$this->bestSale->getTotal()){
            $this->bestSale = $objSale;
        }
    }

    protected function puchRelatoryData($head,$data){
        $this->relatory[]=array($head,$data);
    }

    protected function processRelatory(){
        $salesmanOrderSale = $this->salesman;
        usort($salesmanOrderSale,function($a,$b){
            return $a->getTotalSales() > $b->getTotalSales();
        });
        $this->puchRelatoryData("Quantidade de clientes",count($this->customer));
        $this->puchRelatoryData("Quantidade de vendedores",count($this->salesman));
        $this->puchRelatoryData("Media salarial dos vendedores",$this->salarySum/count($this->salesman));
        $this->puchRelatoryData("ID da melhor venda",$this->bestSale->getSaleId());
        $worstSalesman=null;
        foreach($salesmanOrderSale as $salesman){
            if($worstSalesman==null||$worstSalesman->getTotalSales() == $salesman->getTotalSales()){
                $worstSalesman = $salesman;
            }else{
                break;
            }
            $this->puchRelatoryData("Nome e valor do pior vendedor",$worstSalesman->getName().", ".$worstSalesman->getTotalSales());
        }
    }
}


?>