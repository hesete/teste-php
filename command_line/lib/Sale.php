<?php
namespace App\libraries\FileProcessor;

require_once 'SaleItem.php';

class Sale{
    private $string;
	private $saleId;
    private $salesman;
    private $salesItens=[];
	private $total = 0;

	public function __construct($string){
		$this->string = $string;
		$this->parse();
	}

	public function parse()
	{
        list($ent,$saleId,$strItens,$salesmanId)=str_getcsv($this->string,',','"');
		$arryItems = explode(",", $strItens);

		$this->saleId = $saleId;
		$this->salesman = $salesmanId;
		
		foreach($arryItems as $strItem){
            $saleItem = new SaleItem($strItem);
            $this->total += ($saleItem->getItemQuantity()*$saleItem->getItemPrice());
            $this->salesItens[]=$saleItem;
		}
	}

	public function getTotal(){
		return $this->total;
	}

	public function getSalesman(){
		return $this->salesman;
	}

	public function getSaleId(){
		return $this->saleId;
	}

	public function getSalesItens(){
		return $this->salesItens;
	}
}
?>